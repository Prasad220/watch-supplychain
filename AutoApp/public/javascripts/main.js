
function toManuDash() {
    window.location.href = '/manufacturer';
}

function swalBasic(data) {
    swal.fire({
        // toast: true,
        icon: `${data.icon}`,
        title: `${data.title}`,
        animation: true,
        position: 'center',
        showConfirmButton: true,
        footer: `${data.footer}`,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
        }
    })
}

// function swalDisplay(data) {
//     swal.fire({
//         // toast: true,
//         icon: `${data.icon}`,
//         title: `${data.title}`,
//         animation: false,
//         position: 'center',
//         html: `<h3>${JSON.stringify(data.response)}</h3>`,
//         showConfirmButton: true,
//         timer: 3000,
//         timerProgressBar: true,
//         didOpen: (toast) => {
//             toast.addEventListener('mouseenter', swal.stopTimer)
//             toast.addEventListener('mouseleave', swal.resumeTimer)
//         }
//     }) 
// }

function reloadWindow() {
    window.location.reload();
}




function ManWriteData() {
    event.preventDefault();
    const watch = document.getElementById('watchNumber').value;
    const number = document.querySelector('#model-select').value;
    const brand = document.getElementById('watchBrand').value;
    const color = document.getElementById('watchColor').value;
    const price = document.getElementById('watchPrice').value;
    const dom = document.getElementById('dom').value;
    console.log(watch + number + brand + color + price + dom);

    if (watch.length == 0 || number.length == 0 || brand.length == 0 || color.length == 0 || price.length == 0 || dom.length == 0) {
        const data = {
            title: "You might have missed something",
            footer: "Enter all mandatory fields to add a new car",
            icon: "warning"
        }
        swalBasic(data);
    }
    else {
        fetch('/manuwrite', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ WatchNumb: watch, watchmodel: number, watchBrand: brand, watchColor: color, watchPrice: price, watchDom: dom })
        })
            .then(function (response) {
                if (response.status == 200) {
                    const data = {
                        title: "Success",
                        footer: "Added a new Watch",
                        icon: "success"
                    }
                    swalBasic(data);
                } else {
                    const data = {
                        title: `Watch with Watch Number ${watch} already exists`,
                        footer: "Watch Number must be unique",
                        icon: "error"
                    }
                    swalBasic(data);
                }

            })
            .catch(function (error) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}
function ManQueryData() {

    event.preventDefault();
    const Qwatch = document.getElementById('QueryWatchNumbMan').value;

    console.log(Qwatch);

    if (Qwatch.length == 0) {
        const data = {
            title: "Enter a Valid Qwatch Number",
            footer: "This is a mandatory field",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/manuread', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ QWatchNumb: Qwatch })
        })
            .then(function (response) {
                console.log(response);
                return response.json();
            })
            .then(function (Cardata) {
                dataBuf = Cardata["Cardata"]
                swal.fire({
                    // toast: true,
                    icon: `success`,
                    title: `Current status of Watch with Qwatch ${Qwatch} :`,
                    animation: false,
                    position: 'center',
                    html: `<h3>${dataBuf}</h3>`,
                    showConfirmButton: true,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', swal.stopTimer)
                        toast.addEventListener('mouseleave', swal.resumeTimer)
                    }
                })
            })
            .catch(function (error) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

//Method to get the history of an item
function getItemHistory(watchId) {
    console.log("postalId", watchId)
    window.location.href = '/itemhistory?watchId=' + watchId;
}

function getMatchingOrders(watchId) {
    console.log("watchId", watchId)
    window.location.href = 'matchOrder?watchId=' + watchId;
}

function RegisterWatch() {
    console.log("Entered the register function")
    event.preventDefault();
    const QWatchNumb = document.getElementById('QWatchNumb').value;
    const carOwner = document.getElementById('carOwner').value;
    const regNumber = document.getElementById('regNumber').value;
    console.log(QWatchNumb + carOwner + regNumber);

    if (QWatchNumb.length == 0 || carOwner.length == 0 || regNumber.length == 0) {
        const data = {
            title: "You have missed something",
            footer: "All fields are mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/registerCar', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ QWatchNumb: QWatchNumb, carOwner: carOwner, regNumber: regNumber })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Registered car ${QWatchNumb} to ${carOwner}`,
                        footer: "Registered car",
                        icon: "success"
                    }
                    swalBasic(data)
                } else {
                    const data = {
                        title: `Failed to register car`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function createOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderNumber = document.getElementById('orderNumber').value;
    const watchmodel = document.getElementById('watchModel').value;
    const watchBrand = document.getElementById('watchBrand').value;
    const watchColor = document.getElementById('watchColor').value;
    const watchPrice = document.getElementById('watchPrice').value;
    console.log(orderNumber + watchColor + watchPrice);

    if (orderNumber.length == 0 || watchmodel.length == 0 || watchBrand.length == 0
        || watchColor.length == 0 || watchPrice.length == 0) {
        const data = {
            title: "You have missed something",
            footer: "All fields are mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/createOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderNumber: orderNumber, watchmodel: watchmodel, watchBrand: watchBrand, watchColor: watchColor, watchPrice: watchPrice })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order is created`,
                        footer: "Raised Order",
                        icon: "success"
                    }
                    swalBasic(data)

                } else {
                    const data = {
                        title: `Failed to create order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function readOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderNumber = document.getElementById('ordNum').value;

    console.log(orderNumber);

    if (orderNumber.length == 0) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/readOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderNumber: orderNumber })
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (orderData) {
                dataBuf = orderData["orderData"]
                swal.fire({
                    // toast: true,
                    icon: `success`,
                    title: `Current status of Order : `,
                    animation: false,
                    position: 'center',
                    html: `<h3>${dataBuf}</h3>`,
                    showConfirmButton: true,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', swal.stopTimer)
                        toast.addEventListener('mouseleave', swal.resumeTimer)
                    }
                })
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function matchOrder(orderId, watchId) {
    if (!orderId || !watchId) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)
    } else {
        fetch('/match', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderId, watchId })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order matched successfully`,
                        footer: "Order matched",
                        icon: "success"
                    }
                    swalBasic(data)
                } else {
                    const data = {
                        title: `Failed to match order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })

            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}


function allOrders() {
    window.location.href = '/allOrders';
}


function getEvent() {
    fetch('/event', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(function (response) {
            console.log("Response is ###", response)
            return response.json()
        })
        .then(function (event) {
            dataBuf = event["watchEvent"]
            swal.fire({
                toast: true,
                // icon: `${data.icon}`,
                title: `Event : `,
                animation: false,
                position: 'top-right',
                html: `<h5>${dataBuf}</h5>`,
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            })
        })
        .catch(function (err) {
            swal.fire({
                toast: true,
                icon: `error`,
                title: `Error`,
                animation: false,
                position: 'top-right',
                showConfirmButton: true,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            })
        })
}