var express = require('express');
var router = express.Router();
const { clientApplication } = require('./client');
const { Events } = require('./events')
let eventClient = new Events()
eventClient.contractEventListner("manufacturer", "Admin", "autochannel",
  "WatchesShoppingChaincode", "WatchContract", "addWatchEvent")



/* GET home page. */
router.get('/', function (req, res, next) {
  let retailerClient = new clientApplication();

  retailerClient.generatedAndEvaluateTxn(
    "retailer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "queryAllWatchs"
  )
    .then(watches => {
      const dataBuffer = watches.toString();
      console.log("watches are ", watches.toString())
      const value = JSON.parse(dataBuffer)
      console.log("History DataBuffer is", value)
      res.render('index', { title: 'Watch Shop', itemList: value });
    }).catch(err => {
      res.render("error", {
        message: `Some error occured`,
        callingScreen: "error",
      })
    })
});

router.get('/manufacturer', function (req, res, next) {
  let manufacturerClient = new clientApplication();
  manufacturerClient.generatedAndEvaluateTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "queryAllWatchs"
  ).then(watches => {
    const data = watches.toString();
    const value = JSON.parse(data)
    res.render('manufacturer', { title: 'Manufacturer Dashboard', itemList: value });
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
  })

});
router.get('/dealer', function (req, res, next) {
  res.render('dealer', { title: 'Dealer Dashboard' });
});

router.get('/event', function (req, res, next) {
  console.log("Event Response %%%$$^^$%%$", eventClient.getEvents().toString())
  var event = eventClient.getEvents().toString()
  res.send({ carEvent: event })
  // .then(array => {
  //   console.log("Value is #####", array)
  //   res.send(array);
  // }).catch(err => {
  //   console.log("errors are ", err)
  //   res.send(err)
  // })
  // res.render('index', { title: 'Dealer Dashboard' });
});


router.get('/retailer', function (req, res, next) {
  let retailerClient = new clientApplication();
  retailerClient.generatedAndEvaluateTxn(
    "retailer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "queryAllWatchs"
  )
    .then(watches => {
      const dataBuffer = watches.toString();
      console.log("watches are ", watches.toString())
      const value = JSON.parse(dataBuffer)
      console.log("History DataBuffer is", value)
      res.render('retailer', { title: 'retailer Dashboard', itemList: value });
    }).catch(err => {
      res.render("error", {
        message: `Some error occured`,
        callingScreen: "error",
      })
    })
});



router.get('/addWatchEvent', async function (req, res, next) {
  let retailerClient = new clientApplication();
  const result = await retailerClient.contractEventListner("manufacturer", "Admin", "autochannel",
    "WatchesShoppingChaincode", "addWatchEvent")
  console.log("The result is ####$$$$", result)
  res.render('manufacturer', { view: "carEvents", results: result })
})

router.post('/manuwrite', function (req, res) {

  const watch = req.body.WatchNumb;
  const number = req.body.watchmodel;
  const color = req.body.watchColor;
  const dateofmanufacture = req.body.watchDom;
  const brand = req.body.watchBrand;
  const price = req.body.watchPrice;

  console.log(req)
  // console.log("Request Object",req)
  let ManufacturerClient = new clientApplication();

  ManufacturerClient.generatedAndSubmitTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "createWatch",
    watch, number, color, dateofmanufacture, brand, price

  ).then(message => {
    console.log("Message is $$$$", message)
    res.status(200).send({ message: "Added Watch" })
  }
  )
    .catch(error => {
      console.log("Some error Occured $$$$###", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/manuread', async function (req, res) {
  const Qwatch = req.body.QWatchNumb;
  let ManufacturerClient = new clientApplication();

  ManufacturerClient.generatedAndEvaluateTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "readWatch", Qwatch)
    .then(message => {

      res.status(200).send({ Cardata: message.toString() });
    }).catch(error => {

      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });

})

//  Get History of a car
router.get('/itemhistory', async function (req, res) {
  const watchId = req.query.watchId;

  let retailerClient = new clientApplication();

  retailerClient.generatedAndEvaluateTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "getWatchesHistory", watchId).then(message => {
      const dataBuffer = message.toString();

      const value = JSON.parse(dataBuffer)
      res.render('history', { itemList: value, title: "Watch History" })

    });

})

//Register a car

router.post('/registerCar', async function (req, res) {
  const Qwatch = req.body.QWatchNumb;
  const CarOwner = req.body.carOwner;
  const RegistrationNumber = req.body.regNumber
  let retailerClient = new clientApplication();

  retailerClient.generatedAndSubmitTxn(
    "retailer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "registerCar", Qwatch, CarOwner, RegistrationNumber)
    .then(message => {

      res.status(200).send("Successfully created")
    }).catch(error => {

      res.status(500).send({ error: `Failed to create`, message: `${error}` })
    });

})
// Create order
router.post('/createOrder', async function (req, res) {
  const orderNumber = req.body.orderNumber;
  const watchNumber = req.body.watchmodel;
  const watchColor = req.body.watchColor;
  const watchBrand = req.body.watchBrand;
  const watchPrice = req.body.watchPrice
  let DealerClient = new clientApplication();

  const transientData = {
    number: Buffer.from(watchNumber),
    color: Buffer.from(watchColor),
    brand: Buffer.from(watchBrand),
    price: Buffer.from(watchPrice)
  }

  DealerClient.generatedAndSubmitPDC(
    "dealer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "OrderContract",
    "createOrder", orderNumber, transientData)
    .then(message => {

      res.status(200).send("Successfully created")
    }).catch(error => {

      res.status(500).send({ error: `Failed to create`, message: `${error}` })
    });

})

router.post('/readOrder', async function (req, res) {
  const orderNumber = req.body.orderNumber;
  let DealerClient = new clientApplication();
  DealerClient.generatedAndEvaluateTxn(
    "dealer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "OrderContract",
    "readOrder", orderNumber).then(message => {

      res.send({ orderData: message.toString() });
    }).catch(error => {
      alert('Error occured')
    })

})

//Get all orders
router.get('/allOrders', async function (req, res) {
  let DealerClient = new clientApplication();
  DealerClient.generatedAndEvaluateTxn(
    "dealer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "OrderContract",
    "queryAllOrders", "").then(message => {
      const dataBuffer = message.toString();
      const value = JSON.parse(dataBuffer);
      res.render('orders', { itemList: value, title: "All Orders" })
    }).catch(error => {
      //alert('Error occured')
      console.log(error)
    })

})
//Find matching orders
router.get('/matchOrder', async function (req, res) {
  const watchId = req.query.watchId;

  let retailerClient = new clientApplication();

  retailerClient.generatedAndEvaluateTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "checkMatchingOrders", watchId).then(message => {
      console.log("Message response", message)
      var dataBuffer = message.toString();
      var data = [];
      data.push(dataBuffer, watchId)
      console.log("checkMatchingOrders", data)
      const value = JSON.parse(dataBuffer)
      let array = [];
      if (value.length) {
        for (i = 0; i < value.length; i++) {
          array.push({
            "orderId": `${value[i].Key}`, "watchId": `${watchId}`,
            "number": `${value[i].Record.number}`, "color": `${value[i].Record.color}`,
            "brand": `${value[i].Record.brand}`,
            "price": `${value[i].Record.price}`, "assetType": `${value[i].Record.assetType}`
          })
        }
      }
      console.log("Array value is ", array)
      console.log("Watch id sent", watchId)
      res.render('matchOrder', { itemList: array, title: "Matching Orders" })

    });

})

router.post('/match', async function (req, res) {
  const orderId = req.body.orderId;
  const watchId = req.body.watchId
  let DealerClient = new clientApplication();
  DealerClient.generatedAndSubmitTxn(
    "dealer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "matchOrder", watchId, orderId).then(message => {

      res.status(200).send("Successfully Matched order")
    }).catch(error => {

      res.status(500).send({ error: `Failed to Match Order`, message: `${error}` })
    });

})



module.exports = router;


