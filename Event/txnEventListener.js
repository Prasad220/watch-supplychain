const { EventListener } = require('./event')

let ManufacturerEvent = new EventListener();
ManufacturerEvent.txnEventListener("manufacturer", "Admin", "autochannel",
    "WatchesShoppingChaincode", "WatchContract", "createWatch",
    "watch073", "Titan-eye", "Titan", "White", "11/05/2021", "Titan");