let profile = {
    manufacturer: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/manufacturer.watch.com",
        "CP": "../Network/vars/profiles/watchchannel_connection_for_nodesdk.json"
    },
    dealer: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/dealer.watch.com",
        "CP": "../Network/vars/profiles/watchchannel_connection_for_nodesdk.json"
    }
}
module.exports = { profile }