const { clientApplication } = require('./client');

let dealerClient = new clientApplication();
const transientData = {
    number: Buffer.from('14'),

    color: Buffer.from('White'),
    brand: Buffer.from('Titan'),
    price: Buffer.from('9999'),
    dateofmanufacture: Buffer.from('10/01/2022'),
}
// number,
//             color,
//             dateofmanufacture,
//             brand,
//             price,

dealerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "OrderContract",
    "privateTxn",
    transientData,
    "createOrder",
    "Order-02"
).then(msg => {
    console.log(msg.toString())
});