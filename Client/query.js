const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication()

ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "WatchesShoppingChaincode",
    "WatchContract",
    "queryTxn",
    "",
    "readWatch",
    "watch-05"
).then(message => {
    console.log(message.toString())
})