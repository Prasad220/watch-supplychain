/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const ShoesContract = require('./lib/shoes-contract');

module.exports.ShoesContract = ShoesContract;
module.exports.contracts = [ ShoesContract ];
