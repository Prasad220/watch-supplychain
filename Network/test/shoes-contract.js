/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { ShoesContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('ShoesContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new ShoesContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"shoes 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"shoes 1002 value"}'));
    });

    describe('#shoesExists', () => {

        it('should return true for a shoes', async () => {
            await contract.shoesExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a shoes that does not exist', async () => {
            await contract.shoesExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createShoes', () => {

        it('should create a shoes', async () => {
            await contract.createShoes(ctx, '1003', 'shoes 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"shoes 1003 value"}'));
        });

        it('should throw an error for a shoes that already exists', async () => {
            await contract.createShoes(ctx, '1001', 'myvalue').should.be.rejectedWith(/The shoes 1001 already exists/);
        });

    });

    describe('#readShoes', () => {

        it('should return a shoes', async () => {
            await contract.readShoes(ctx, '1001').should.eventually.deep.equal({ value: 'shoes 1001 value' });
        });

        it('should throw an error for a shoes that does not exist', async () => {
            await contract.readShoes(ctx, '1003').should.be.rejectedWith(/The shoes 1003 does not exist/);
        });

    });

    describe('#updateShoes', () => {

        it('should update a shoes', async () => {
            await contract.updateShoes(ctx, '1001', 'shoes 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"shoes 1001 new value"}'));
        });

        it('should throw an error for a shoes that does not exist', async () => {
            await contract.updateShoes(ctx, '1003', 'shoes 1003 new value').should.be.rejectedWith(/The shoes 1003 does not exist/);
        });

    });

    describe('#deleteShoes', () => {

        it('should delete a shoes', async () => {
            await contract.deleteShoes(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a shoes that does not exist', async () => {
            await contract.deleteShoes(ctx, '1003').should.be.rejectedWith(/The shoes 1003 does not exist/);
        });

    });

});
