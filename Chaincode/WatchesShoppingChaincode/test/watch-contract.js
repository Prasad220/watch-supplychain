/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { WatchContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('WatchContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new WatchContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"watch 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"watch 1002 value"}'));
    });

    describe('#watchExists', () => {

        it('should return true for a watch', async () => {
            await contract.watchExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a watch that does not exist', async () => {
            await contract.watchExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createWatch', () => {

        it('should create a watch', async () => {
            await contract.createWatch(ctx, '1003', 'watch 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"watch 1003 value"}'));
        });

        it('should throw an error for a watch that already exists', async () => {
            await contract.createWatch(ctx, '1001', 'myvalue').should.be.rejectedWith(/The watch 1001 already exists/);
        });

    });

    describe('#readWatch', () => {

        it('should return a watch', async () => {
            await contract.readWatch(ctx, '1001').should.eventually.deep.equal({ value: 'watch 1001 value' });
        });

        it('should throw an error for a watch that does not exist', async () => {
            await contract.readWatch(ctx, '1003').should.be.rejectedWith(/The watch 1003 does not exist/);
        });

    });

    describe('#updateWatch', () => {

        it('should update a watch', async () => {
            await contract.updateWatch(ctx, '1001', 'watch 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"watch 1001 new value"}'));
        });

        it('should throw an error for a watch that does not exist', async () => {
            await contract.updateWatch(ctx, '1003', 'watch 1003 new value').should.be.rejectedWith(/The watch 1003 does not exist/);
        });

    });

    describe('#deleteWatch', () => {

        it('should delete a watch', async () => {
            await contract.deleteWatch(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a watch that does not exist', async () => {
            await contract.deleteWatch(ctx, '1003').should.be.rejectedWith(/The watch 1003 does not exist/);
        });

    });

});
