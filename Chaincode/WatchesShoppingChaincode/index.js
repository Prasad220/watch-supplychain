/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const WatchContract = require('./lib/watch-contract');
const OrderContract=require('./lib/order-contract');

module.exports.WatchContract = WatchContract;
module.exports.OrderContract=OrderContract
module.exports.contracts = [ WatchContract,OrderContract ];
