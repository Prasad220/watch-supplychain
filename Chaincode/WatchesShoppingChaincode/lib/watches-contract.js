/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class WatchContract extends Contract {

    async watchExists(ctx, watchId) {
        const buffer = await ctx.stub.getState(watchId);
        return (!!buffer && buffer.length > 0);
    }

    async createWatch(ctx, watchId, value) {
        const exists = await this.watchExists(ctx, watchId);
        if (exists) {
            throw new Error(`The watch ${watchId} already exists`);
        }
        const asset = { value };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(watchId, buffer);
    }

    async readWatch(ctx, watchId) {
        const exists = await this.watchExists(ctx, watchId);
        if (!exists) {
            throw new Error(`The watch ${watchId} does not exist`);
        }
        const buffer = await ctx.stub.getState(watchId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateWatch(ctx, watchId, newValue) {
        const exists = await this.watchExists(ctx, watchId);
        if (!exists) {
            throw new Error(`The watch ${watchId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(watchId, buffer);
    }

    async deleteWatch(ctx, watchId) {
        const exists = await this.watchExists(ctx, watchId);
        if (!exists) {
            throw new Error(`The watch ${watchId} does not exist`);
        }
        await ctx.stub.deleteState(watchId);
    }

}

module.exports = WatchContract;
