/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class WatchContract extends Contract {

    async watchExists(ctx, watchId) {
        const buffer = await ctx.stub.getState(watchId);
        return (!!buffer && buffer.length > 0);
    }

    async createWatch(ctx, watchId, number, color, dateofmanufacture, brand, price) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-watch-com') {
            const exists = await this.watchExists(ctx, watchId);
            if (exists) {
                throw new Error(`The watch ${watchId} already exists`);
            }
            const asset = {
                number,
                color,
                dateofmanufacture,
                brand,
                price,
                assetType: 'watch',
            };
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(watchId, buffer);

            let addWatchEventData = { Type: 'Watch creation', Model: brand };
            await ctx.stub.setEvent('addWatchEvent', Buffer.from(JSON.stringify(addWatchEventData)));

        }
        else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

    async readWatch(ctx, watchId) {
        const exists = await this.watchExists(ctx, watchId);
        if (!exists) {
            throw new Error(`The watch ${watchId} does not exist`);
        }
        const buffer = await ctx.stub.getState(watchId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateWatch(ctx, watchId, newValue) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-watch-com') {
            const exists = await this.watchExists(ctx, watchId);
            if (!exists) {
                throw new Error(`The watch ${watchId} does not exist`);
            }
            const asset = { value: newValue };
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(watchId, buffer);
        } else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

    async deleteWatch(ctx, watchId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-watch-com') {
            const exists = await this.watchExists(ctx, watchId);
            if (!exists) {
                throw new Error(`The watch ${watchId} does not exist`);
            }
            await ctx.stub.deleteState(watchId);
        }
        else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }
    async queryAllWatchs(ctx) {
        const queryString = {
            selector: {
                assetType: 'watch',

            },
            sort: [{ price: "asc" }],
        };
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result);
    }
    async queryAllWatchsByBrand(ctx, brandName) {
        const queryString = {
            selector: {
                brand: brandName,

            },
            sort: [{ price: "asc" }],
        };
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result);
    }

    async getWatchsHistory(ctx, carId) {
        let resultIterator = await ctx.stub.getHistoryForKey(carId);
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result);
    }
    async getAllResults(iterator, isHistory) {
        let allResult = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                if (isHistory && isHistory == true) {
                    jsonRes.TxId = res.value.txId;
                    jsonRes.timestamp = res.value.timestamp;
                    jsonRes.Value = JSON.parse(res.value.value.toString());

                }
                else {
                    jsonRes.Key = res.value.key
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)
            }
        }
        await iterator.close()
        return allResult;

    }




}

module.exports = WatchContract;
